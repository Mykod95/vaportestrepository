//
//  Person.swift
//  App
//
//  Created by Miguel Vieira on 17/08/18.
//

import Vapor
import FluentSQLite

struct Person : Codable
{
    var id : Int?
    var name : String
    
    init(name : String) {
        self.name = name;
        
    }
}

extension Person : Model
{
    typealias Database = SQLiteDatabase;
    typealias ID = Int;
    static var idKey : IDKey = \Person.id //primary key = person.id
}

extension Person : SQLiteModel {}

extension Person : Content {}

extension Person : Migration {}

