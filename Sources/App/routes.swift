import Vapor
import Fluent

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "Hello, world!" example
    router.get("hello") { req in
        return "Hello, world!"
    }
    
    router.get("hello", "people") { req in
        return "Hello, People!";
    }
    
    router.get("hello", String.parameter) { req -> String in
        let returnString = try "Hello, \(req.parameters.next(String.self))";
        return returnString;
    }
    
    //31m
    router.post("api", "v1", "person") { req -> Future<Person> in
        return try req.content.decode(Person.self).flatMap(to: Person.self)
        {
            person in
                return person.save(on: req);
        }
    }
    
    router.get("api", "v1", "allpeople") { req -> Future<[Person]> in
        Person.query(on: req).all();
    }
    
    router.get("api", "v1", "allpeople") { req -> Future<[Person]> in
        
        switch req.query[String.self, at: "sort"]
        {
        case "asc":
            return Person.query(on: req).sort(\Person.name, .ascending).all();
        case "desc":
            return Person.query(on: req).sort(\Person.name, .descending).all();
        default:
            return Person.query(on: req).all();
        }
        
    }
    
    /*
 return Person.query(on: req).sort(\Person.name, .descending).all().map(to: [String].self) {
 peopleNames in
 return peopleNames.map({ $0.name })
 };
 */
    router.get("api", "v1", "allpeople", "names") { req -> Future<[String]> in
        
        switch req.query[String.self, at: "sort"]
        {
        case "asc":
            return Person.query(on: req).sort(\Person.name, .ascending).all().map(to: [String].self) {
                people in
                
                var names = [String]()
                
                for person in people
                {
                    names.append(person.name);
                }
                return names
            };
        case "desc":
            return Person.query(on: req).sort(\Person.name, .descending).all().map(to: [String].self) {
                people in
                
                var names = [String]()
                
                for person in people
                {
                    names.append(person.name);
                }
                return names
            };
        default:
            return Person.query(on: req).all().map(to: [String].self) {
                people in
                
                var names = [String]()
                
                for person in people
                {
                    names.append(person.name);
                }
                return names
            };
        }
    }
 
    router.get("api", "v1", "allpeople", "rand") { req -> Future<Person> in
        
        return Person.query(on: req).all().flatMap(to: Person.self)
        {
            people in
            
            let randNo : Int;
            
            #if os(Linux) //arc4Random não funciona em linux
                srandom(UInt32(time(nil)))
                randNo = Int(random() % people.count) + 1
            #else
                randNo = Int(arc4random_uniform(UInt32(people.count)) + 1)
            #endif
            
            return Person.query(on: req).filter(\.id == randNo).first().map(to: Person.self)
            {
                person in
                
                    guard let person = person else { throw Abort(.notFound)
                
                    }
                
                return person;
            }
        }
    }
    
    router.get("api", "v1", "allpeople", "rand", "names") { req -> Future<String> in
        
        return Person.query(on: req).all().flatMap(to: String.self)
        {
            people in
            
            let randNo : Int;
            
            #if os(Linux) //arc4Random não funciona em linux
            srandom(UInt32(time(nil)))
            randNo = Int(random() % people.count) + 1
            #else
            randNo = Int(arc4random_uniform(UInt32(people.count)) + 1)
            #endif
            
            return Person.query(on: req).filter(\.id == randNo).first().map(to: String.self)
            {
                person in
                
                guard let person = person else { throw Abort(.notFound)
                    
                }
                
                return person.name;
            }
        }
    }
    
    
    
    // Example of configuring a controller
 /*   let todoController = TodoController()
    router.get("todos", use: todoController.index)
    router.post("todos", use: todoController.create)
    router.delete("todos", Todo.parameter, use: todoController.delete)
 */
}
